## SIAK-NG Scraper
Note: Use AT YOUR OWN RISK

A simple API that scrapes data from your SIAK-NG profile and returns all of your scores in a JSON format.

To use simply send a POST request to [https://nilai-siak.herokuapp.com/score](https://nilai-siak.herokuapp.com/score) or [http://nilai-siak.herokuapp.com/gpa_result](http://nilai-siak.herokuapp.com/gpa_result) with these parameters using the following headers:
 1. username [your username]
 2. password [your password]
 3. detail_status [Only for Scores and Details and is Optional, fill with any String to see details on the response]