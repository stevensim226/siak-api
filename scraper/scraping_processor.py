import requests
from bs4 import BeautifulSoup
from lxml import html

def check_float(str_object):
    try:
        float(str_object)
        return True
    except:
        return False

def get_hidden_gpa(username, password):
    with requests.Session() as session:
        # Get auth string from form
        req = session.get("https://sso.ui.ac.id/cas/login?service=http%3A%2F%2Fbeasiswa.ui.ac.id%2Fapps%2Flogin%2Findex")
        soup = BeautifulSoup(req.text,"html.parser")

        # stuff auth strings into dictionary
        auth_stuff = {}
        hidden_input = soup.select("input")[2:]
        for input_box in hidden_input:
            auth_stuff[input_box["name"]] = input_box["value"]

        auth_stuff["username"] = username
        auth_stuff["password"] = password

        req = session.post(
            "https://sso.ui.ac.id/cas/login?service=http%3A%2F%2Fbeasiswa.ui.ac.id%2Fapps%2Flogin%2Findex",
            auth_stuff
        )

        if "Login Failed" in req.text:
            return {"status" : "Failed"}
        
        # Send to 
        req = session.get("http://beasiswa.ui.ac.id/apps/pendaftaranrolependaftar/inputDataPelamar?idPaket=181")
        soup = BeautifulSoup(req.text,"html.parser")

        result = {
            "name": soup.select_one("#Pelamar_nama")["value"],
            "npm" : soup.select_one("#Pelamar_npm")["value"],
            "ipk": soup.select_one("#Pelamar_ipk")["value"],
        }
        return result

def get_all_scores(username, password, request="detail"):
    with requests.Session() as session:
        # Post to login
        req = session.post(
            "https://academic.ui.ac.id/main/Authentication/",
            {
                "u" : username,
                "p" : password
            }
        )

        if "Login Failed" in req.text:
            return False

        session.get("https://academic.ui.ac.id/main/Authentication/ChangeRole")

        # Data akademis page
        req = session.get("https://academic.ui.ac.id/main/Academic/HistoryByTerm")

        soup = BeautifulSoup(req.text,"html.parser")

        nilai = soup.select("td:nth-child(8)")
        nama_matkul = soup.select("td:nth-child(4)")

        nilai = list(map(lambda x : float(x.text) if check_float(x.text) else x.text, nilai))
        nama_matkul = list(map(lambda x : x.text, nama_matkul))

        detail_links = list(map(lambda x : x["href"] ,soup.select(".ce+ .ce a")))

        if request != "detail":
            return dict(zip(nama_matkul, nilai))

        nilai = {}

        for index in range(len(detail_links)):
            new_req = session.get("https://academic.ui.ac.id/main/Academic/" + detail_links[index])
            new_soup = BeautifulSoup(new_req.text,"html.parser")
            komponen_list = list(map(lambda x : x.text, new_soup.select(".box+ .box td:nth-child(1)")))

            # Converts to float if not "Not published"
            nilai_list = list(map(lambda x : float(x.text) if check_float(x.text) else x.text, new_soup.select(".box+ .box td:nth-child(3)")))

            if len(komponen_list) == 0 and len(nilai_list) == 0:
                komponen_list = list(map(lambda x : x.text, new_soup.select(".box~ .box td:nth-child(1)")))
                nilai_list = list(map(lambda x : float(x.text) if check_float(x.text) else x.text, new_soup.select(".ce+ .ce")))

            nilai[nama_matkul[index]] = dict(zip(komponen_list, nilai_list))

        return nilai

def get_all_combined(username, password):
    main_result = {
        "status" : "success",
        "scores" : [],
        "details" : []
    }

    with requests.Session() as session:
    # Post to login
        req = session.post(
            "https://academic.ui.ac.id/main/Authentication/",
            {
                "u" : username,
                "p" : password
            }, verify = False
        )

        if "Login Failed" in req.text:
            return {"status" : "failed"}

        session.get("https://academic.ui.ac.id/main/Authentication/ChangeRole")

        # Data akademis page
        req = session.get("https://academic.ui.ac.id/main/Academic/HistoryByTerm")

        soup = BeautifulSoup(req.text,"html.parser")

        matkul_name = soup.select("td:nth-child(4)")
        matkul_name = list(map(lambda x : x.text, matkul_name))

        scores_number = soup.select("td:nth-child(8)")
        scores_number = list(map(lambda x : x.text, scores_number))

        scores_letter = soup.select(".ce:nth-child(8) , td~ .ri+ .ce")
        scores_letter = list(map(lambda x : x.text, scores_letter))

        for index in range(len(matkul_name)):
            obj = {}
            obj["name"] = matkul_name[index]
            obj["grade_number"] = scores_number[index]
            obj["grade_letter"] = scores_letter[index]
            main_result["scores"].append(obj)

        detail_links = list(map(lambda x : x["href"] ,soup.select(".ce+ .ce a")))

        for detail_index in range(len(detail_links)):
            obj = {}

            new_req = session.get("https://academic.ui.ac.id/main/Academic/" + detail_links[detail_index])
            new_soup = BeautifulSoup(new_req.text,"html.parser")
            komponen_list = list(map(lambda x : x.text, new_soup.select(".box+ .box td:nth-child(1)")))

            
            nilai_list = list(map(lambda x : x.text, new_soup.select(".box+ .box td:nth-child(3)")))
            obj["scores"] = []

            # If the conversion table doesn't appear
            if len(komponen_list) == 0 and len(nilai_list) == 0:
                komponen_list = list(map(lambda x : x.text, new_soup.select(".box~ .box td:nth-child(1)")))
                nilai_list = list(map(lambda x : x.text, new_soup.select(".ce+ .ce")))
            
            # Loop every component and scores
            for index in range(len(komponen_list)) :
                obj["scores"].append({"component" : komponen_list[index], "score" : nilai_list[index]})

            obj["name"] = matkul_name[detail_index]
            main_result["details"].append(obj)
        return main_result

if __name__ == "__main__":
    pass