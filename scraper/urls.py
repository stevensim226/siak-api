from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.input_credentials, name="input_credentials"),
    path("score", views.show_scores, name="show_scores"),
    path("gpa", views.input_credentials_gpa, name="input_credentials_gpa"),
    path("gpa_result", views.show_gpa, name="show_gpa"),
    path("scores_combined", views.input_credentials_combined, name="input_credentials_combined"),
    path("scores_combines_result", views.show_combined_result, name="scores_combined_result")
]
