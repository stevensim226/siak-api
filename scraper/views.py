from django.shortcuts import render, HttpResponse, redirect
from django.http import JsonResponse
from .scraping_processor import get_all_scores, get_hidden_gpa, get_all_combined
from django.views.decorators.csrf import csrf_exempt

def input_credentials(request):
    return render(request, "scraper/input_credentials.html")

def input_credentials_gpa(request):
    return render(request, "scraper/input_credentials_gpa.html")

def input_credentials_combined(request):
    return render(request, "scraper/input_credentials_combined.html")

@csrf_exempt
def show_gpa(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        
        result = get_hidden_gpa(username, password)
        try:
            if result["status"] == "Failed":
                return HttpResponse("Failed to get data, Check if password / username is correct")
        except:
            return JsonResponse(result)
    return redirect("input_credentials_gpa")


@csrf_exempt
def show_scores(request):
    
    if request.method == "POST":
        try:
            if request.POST["detail_status"]:
                context = {
                    "scores" : get_all_scores(request.POST["username"], request.POST["password"]),
                }  
        except:
            context = {
                "scores" : get_all_scores(request.POST["username"], request.POST["password"], "not detail"),
            }
        if context["scores"]:
            return JsonResponse(context["scores"])
        return HttpResponse("Failed to get data, Check if password / username is correct")
        #return render(request, "scraper/show_scores.html", context)
    return redirect("input_credentials")

@csrf_exempt
def show_combined_result(request):
    if request.method == "POST":
        try:
            username = request.POST["username"]
            password = request.POST["password"]
            result = get_all_combined(username, password)
            if result:
                return JsonResponse(result)
        except Exception as err:
            return JsonResponse({ "error" : str(err) })
    return redirect("input_credentials_combined")
